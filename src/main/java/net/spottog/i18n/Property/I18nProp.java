package net.spottog.i18n.Property;

import java.util.Locale;

import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import net.spottog.i18n.TextSave.FakeSave;
import net.spottog.i18n.global.I18n;
import net.spottog.i18n.global.ITextSave;

/**
 * Ein Standard Textobejekt, welche zusätzliche Methoden Implementiert, welche es ermöglichen ein String Property zu generieren.
 * Über des Sring PRoperty, ist es möglich eine JavaFX oberfläche nazuweißen, sich zu Aktualisieren.
 * Wenn eine geänderte Sprache, bzw. Texte angezeigt werden kann dies mittels einem Einzelnen Methodenaufruf erfolgen.
 *
 */
public class I18nProp extends I18n {
	private static ConcurrentHashMap<I18n, StringProperty> Save = new ConcurrentHashMap<I18n, StringProperty>();
	/**
	 * Erstellt ein Standard Text Property
	 * @param text der Text
	 * @param cultureInfo Die Sprache, in welche der Text gleich Gespeichert wird.
	 */
	public I18nProp(String text, Locale cultureInfo) {
		super(text, cultureInfo);
	}
	/**
	 * Erstellt ein Standard Text Property
	 * @param text der Text
	 * @param second Ein Weiteres Text Element, als Verkettung
	 * @param cultureInfo Die Sprache, in welche der Text gleich Gespeichert wird.
	 */
	public I18nProp(String text, I18n second, Locale cultureInfo) {
		super(text, second, cultureInfo);
	}
	/**
	 * Erstellt ein Standard Property 
	 * @param text der Text
	 * @param second Ein Weiteres Text Element, als Verkettung
	 */
	public I18nProp(String text, I18n second) {
		super(text, second);
	}
	/**
	 * Erstellt ein Standard Text Property
	 * @param text der Text
	 */
	public I18nProp(String text) {
		super(text);
	}
	/**
	 * Erzeugt das StringProperty, welches in die Oberfläche eingebunden werden kann.
	 * @return das Registierte Text Property
	 */
	public StringProperty getProperty() {
		return Save.computeIfAbsent(this, new Function<I18n, StringProperty>() {
			public StringProperty apply(I18n k) {
				return new SimpleStringProperty(k.toString());
			}
		});
	}
	/**
	 * Weißt alle erzeugten Text Property Elemente an sich selbst zu Aktualisieren.
	 */
	public static void FireChangeLangugage() {
		for(java.util.Map.Entry<I18n, StringProperty> sp : Save.entrySet()) {
			sp.getValue().set(sp.getKey().toString());
		}
	}
	/**
	 * Weißt das Grafik Freamwork alle elemente neu zu zeichnen auch wenn sich der Text nicht geändert hat.
	 */
	public static void Repaint() {
		for(java.util.Map.Entry<I18n, StringProperty> sp : Save.entrySet()) {
			sp.getValue().set("");
		}
		FireChangeLangugage();
	}
	/**
	 * Macht das JavaFX System dem net.spottog.I18n Sytem bekannt.
	 */
	public static void Init(Stage stage) {
        stage.addEventHandler(WindowEvent.WINDOW_SHOWN, new EventHandler<Event>() {
			public void handle(Event event) {
				Repaint();
			}
		});
	}
	/**
	 * Bindet  ein JavaFX bedienelement an das I18n Property und initialisiert den Text Save
	 * @param control das zu bindende Element
	 * @param save Initialiert den Text Save
	 */
	public static void Init(Stage stage, ITextSave TextSave) {
		I18n.Save = TextSave;
		Init(stage);
		
	}
	/**
	 * Bindet  ein JavaFX bedienelement an das I18n Property
	 * @param control das zu bindende Element
	 */
	public void addListener(final InvalidationListener listener) {
		getProperty().addListener(listener);
	}

}
